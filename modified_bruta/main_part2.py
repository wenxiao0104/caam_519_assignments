#! /usr/bin/env python
import numpy as np
import os
import subprocess
from subprocess import Popen, PIPE

from pylab import legend,loglog,show,title,xlabel,ylabel

comm = '~/420/hw4/bin/ex5'
direc = '~/420/hw4/tryss.txt'

text_file = open("Output_lu.txt","w")

its	=	[]
err_l2	=	[]
err_inf	=	[]
rec	=	[]
k_max 	=	5

flops_LU	=	[]
flops_GAMG	=	[]
err_l2_LU	=	[]
err_l2_GAMG	=	[]

#------------------LU----------------------
for k in range(k_max): 
	k=k+0
	rec = []
	out_bytes = subprocess.check_output(['./bin/ex5','-mms','1','snes_type','newtonls','-da_refine','1','-ksp_rtol','1e-9','-pc_type','lu','-snes_grid_sequence',str(k)])
	text_file.write(out_bytes)
	flag = 1
	its_temp=0
	for k2 in range(len(out_bytes)-0):		
		if(out_bytes[k2]==' '):
			flag = 0
		if(flag==1):
			its_temp=its_temp*10+ord(out_bytes[k2])-48
	its.append(its_temp)
	flag = 1
	flag1 = 0
	l2_temp = 0.0
	mult = 0
	for k2 in range(len(out_bytes)):
		if((flag1==1)&(out_bytes[k2]==' ')):
			flag = 0
		if(flag1&flag):
			mult=mult*10
			if(out_bytes[k2]=='.'):
				mult=1
			else:
				l2_temp=l2_temp*10+ord(out_bytes[k2])-48
				#print"every:",out_bytes[k2]
		if((flag1==0)&(out_bytes[k2]==' ')):
			flag1=1

	err_l2.append(l2_temp/mult)

	flag = 1
	flag1 = 0
	inf_temp = 0.0
	mult = 0
	for k2 in range(len(out_bytes)):
		if((flag1==2)&(out_bytes[k2]==' ')):
			flag = 0
		
		if((flag1==2)&flag):
			mult=mult*10
			if(out_bytes[k2]=='.'):
				mult=1
			else:
				inf_temp=inf_temp*10+ord(out_bytes[k2])-48
				#print"every:",out_bytes[k2]
		if((out_bytes[k2]==' ')):
			flag1+=1

	err_inf.append(inf_temp/mult)
print"LU:"

for k in range(len(its)):
	print"Its: ",its[k]," L2_err: ",err_l2[k]," Inf_err: ",err_inf[k]


#------------------------GAMG-----------------------

text_file = open("Output_gamg.txt","w")

its_gamg	=	[]
err_l2_gamg	=	[]
err_inf_gamg	=	[]
rec	=	[]


for k in range(k_max): 
	k=k+0
	rec = []

	out_bytes = subprocess.check_output(['./bin/ex5','-mms','1','snes_type','newtonls','-da_refine','1','-ksp_rtol','1e-9','-ksp_type','gmres','-pc_type','gamg','-snes_grid_sequence',str(k)])
	text_file.write(out_bytes)
	flag = 1
	its_temp=0
	for k2 in range(len(out_bytes)-0):		
		if(out_bytes[k2]==' '):
			flag = 0
		if(flag==1):
			its_temp=its_temp*10+ord(out_bytes[k2])-48
	its_gamg.append(its_temp)
	flag = 1
	flag1 = 0
	l2_temp = 0.0
	mult = 0
	for k2 in range(len(out_bytes)):
		if((flag1==1)&(out_bytes[k2]==' ')):
			flag = 0
		if(flag1&flag):
			mult=mult*10
			if(out_bytes[k2]=='.'):
				mult=1
			else:
				l2_temp=l2_temp*10+ord(out_bytes[k2])-48
				#print"every:",out_bytes[k2]
		if((flag1==0)&(out_bytes[k2]==' ')):
			flag1=1

	err_l2_gamg.append(l2_temp/mult)

	flag = 1
	flag1 = 0
	inf_temp = 0.0
	mult = 0
	for k2 in range(len(out_bytes)):
		if((flag1==2)&(out_bytes[k2]==' ')):
			flag = 0
		
		if((flag1==2)&flag):
			mult=mult*10
			if(out_bytes[k2]=='.'):
				mult=1
			else:
				inf_temp=inf_temp*10+ord(out_bytes[k2])-48
				#print"every:",out_bytes[k2]
		if((out_bytes[k2]==' ')):
			flag1+=1

	err_inf_gamg.append(inf_temp/mult)

#---------------------Extract Flops---------------------
flops_LU 	=	[]
flops_GAMG	=	[]



for k in range(k_max): 
	modname	='perfLU%d'%k
	#print"modename:",modname
	options	=['-mms','1','snes_type','newtonls','-da_refine','1','-log_view',':%s.py:ascii_info_detail' %modname,'-pc_type','lu','-snes_grid_sequence',str(k),'-ksp_rtol','1e-9']
	os.system('./bin/ex5 '+' '.join(options))
	perfmod	=__import__(modname)
	flops_LU.append(perfmod.Stages['Main Stage']['summary'][0]['flops'])
	modname	='perfGAMG%d'%k
	options	=['-mms','1','snes_type','newtonls','-da_refine','1','-snes_grid_sequence',str(k),'-log_view',':%s.py:ascii_info_detail' %modname,'-ksp_type','gmres','-pc_type','gamg','-ksp_rtol','1e-9']
	os.system('./bin/ex5 '+' '.join(options))
	perfmod	=__import__(modname)
	flops_GAMG.append(perfmod.Stages['Main Stage']['summary'][0]['flops'])


#print"check: ",flops_GAMG
#print"check: ",flops_LU


print "GAMG"
#print "Iterations:",its
#print "L2_Norm Error:",err_l2
#print "Inf_Norm Error:",err_inf
for k in range(len(its)):
	print"Its: ",its_gamg[k]," L2_err: ",err_l2_gamg[k]," Inf_err: ",err_inf_gamg[k]




#----------------------Plot--------------------------





loglog(flops_LU,err_l2,'r',flops_GAMG,err_l2,'g')
title('Work-precision (LU& GMRES/GAMG) of modified Bruta Func mms 1')
xlabel('Flops executed')
ylabel('Solution l_2 Error')
legend(['err_l2_LU','err_l2_GAMG'])
show()



text_file.close()





