#! /usr/bin/env python
import numpy as np
import os
import subprocess
from subprocess import Popen, PIPE

from pylab import legend,loglog,show,title,xlabel,ylabel

comm = '~/420/hw4/bin/ex5'
direc = '~/420/hw4/tryss.txt'

text_file = open("Output_mms_1.txt","w")

its	=	[]
err_l2	=	[]
err_inf	=	[]
rec	=	[]

#------------------MMS 1----------------------
for k in range(6): 
	k=k+0
	rec = []

	out_bytes = subprocess.check_output(['./bin/ex5','-mms','1','snes_type','newtonls','-da_refine','1','-ksp_rtol','1e-9','-pc_type','lu','-snes_grid_sequence',str(k)])
	text_file.write(out_bytes)
	flag = 1
	its_temp=0
	for k2 in range(len(out_bytes)-0):		
		if(out_bytes[k2]==' '):
			flag = 0
		if(flag==1):
			its_temp=its_temp*10+ord(out_bytes[k2])-48
	its.append(its_temp)
	flag = 1
	flag1 = 0
	l2_temp = 0.0
	mult = 0
	for k2 in range(len(out_bytes)):
		if((flag1==1)&(out_bytes[k2]==' ')):
			flag = 0
		if(flag1&flag):
			mult=mult*10
			if(out_bytes[k2]=='.'):
				mult=1
			else:
				l2_temp=l2_temp*10+ord(out_bytes[k2])-48
				#print"every:",out_bytes[k2]
		if((flag1==0)&(out_bytes[k2]==' ')):
			flag1=1

	err_l2.append(l2_temp/mult)

	flag = 1
	flag1 = 0
	inf_temp = 0.0
	mult = 0
	for k2 in range(len(out_bytes)):
		if((flag1==2)&(out_bytes[k2]==' ')):
			flag = 0
		
		if((flag1==2)&flag):
			mult=mult*10
			if(out_bytes[k2]=='.'):
				mult=1
			else:
				inf_temp=inf_temp*10+ord(out_bytes[k2])-48
				#print"every:",out_bytes[k2]
		if((out_bytes[k2]==' ')):
			flag1+=1

	err_inf.append(inf_temp/mult)

print "Iterations:",its
print "L2_Norm Error:",err_l2
print "Inf_Norm Error:",err_inf
for k in range(len(its)):
	print"Its: ",its[k]," L2_err: ",err_l2[k]," Inf_err: ",err_inf[k]

# -------------------Results of MMS2--------------------



text_file = open("Output_mms_2.txt","w")

its_mms2	=	[]
err_l2_mms2	=	[]
err_inf_mms2	=	[]
rec_mms2	=	[]


for k in range(6): 
	k=k+0
	rec = []

	out_bytes = subprocess.check_output(['./bin/ex5','-mms','2','snes_type','newtonls','-da_refine','1','-ksp_rtol','1e-9','-pc_type','lu','-snes_grid_sequence',str(k)])
	text_file.write(out_bytes)
	flag = 1
	its_temp=0
	for k2 in range(len(out_bytes)-0):		
		if(out_bytes[k2]==' '):
			flag = 0
		if(flag==1):
			its_temp=its_temp*10+ord(out_bytes[k2])-48
	its_mms2.append(its_temp)
	flag = 1
	flag1 = 0
	l2_temp = 0.0
	mult = 0
	for k2 in range(len(out_bytes)):
		if((flag1==1)&(out_bytes[k2]==' ')):
			flag = 0
		if(flag1&flag):
			mult=mult*10
			if(out_bytes[k2]=='.'):
				mult=1
			else:
				l2_temp=l2_temp*10+ord(out_bytes[k2])-48
				#print"every:",out_bytes[k2]
		if((flag1==0)&(out_bytes[k2]==' ')):
			flag1=1

	err_l2_mms2.append(l2_temp/mult)

	flag = 1
	flag1 = 0
	inf_temp = 0.0
	mult = 0
	for k2 in range(len(out_bytes)):
		if((flag1==2)&(out_bytes[k2]==' ')):
			flag = 0
		
		if((flag1==2)&flag):
			mult=mult*10
			if(out_bytes[k2]=='.'):
				mult=1
			else:
				inf_temp=inf_temp*10+ord(out_bytes[k2])-48
				#print"every:",out_bytes[k2]
		if((out_bytes[k2]==' ')):
			flag1+=1

	err_inf_mms2.append(inf_temp/mult)

print "Iterations:",its
print "L2_Norm Error:",err_l2
print "Inf_Norm Error:",err_inf
for k in range(len(its)):
	print"Its: ",its_mms2[k]," L2_err: ",err_l2_mms2[k]," Inf_err: ",err_inf_mms2[k]


# -----------------------Ploting------------------------


loglog(its,err_l2,'r',its,err_inf,'g',its_mms2,err_l2_mms2,'r--',its_mms2,err_inf_mms2,'g--')
title('Convergence of modified Bruta Func mms 1 & 2')
xlabel('Number of Dof')
ylabel('Solution Error')
legend(['err_l2_mms1','err_inf_mms1','err_l2_mms2','err_inf_mms2'])
show()
#print "Check again:%f", data[0]

#sys.stdout = orig_stdout
text_file.close()





