#! /usr/bin/env python
import os

sizes_ILU = []
sizes_GAMG = []
times_ILU = []
times_GAMG = []
for k in range(5): 
	Nx	=10 *2**k
	modname	='perf%d'%k
	options	=['-da_grid_x',str(Nx),'-da_grid_y',str(Nx),'-log_view',':%s.py:ascii_info_detail' %modname,'-ksp_type','gmres','-pc_type','ilu']
	os.system('./bin/ex5 '+' '.join(options))
	perfmod	=__import__(modname)
	sizes_ILU.append(Nx ** 2)
	times_ILU.append(perfmod.Stages['Main Stage']['KSPSolve'][0]['time'])
	modname	='perf_new%d'%k
	options	=['-da_grid_x',str(Nx),'-da_grid_y',str(Nx),'-log_view',':%s.py:ascii_info_detail' %modname,'-ksp_type','gmres','-pc_type','gamg']
	os.system('./bin/ex5 '+' '.join(options))
	perfmod	=__import__(modname)
	sizes_GAMG.append(Nx ** 2)
	times_GAMG.append(perfmod.Stages['Main Stage']['KSPSolve'][0]['time'])
	
	#print zip(sizes,times)
	#print zip(':%s.py:ascii_info_detail' %modname)

from pylab import legend, plot, loglog, show,title,xlabel,ylabel
p1, =plot(sizes_ILU,times_ILU)
p2, =plot(sizes_GAMG,times_GAMG)
legend([p1,p2],["KSP/ILU","KSP/GAMG"])
title('Linear solver time')
xlabel('Problem Size $N$')
ylabel('Time (s)')
show()

p1, =loglog(sizes_ILU,times_ILU)
p2, =loglog(sizes_GAMG,times_GAMG)
legend([p1,p2],["KSP/ILU","KSP/GAMG"])
title('Linear solver time (loglog)')
xlabel('Problem Size $N$')
ylabel('Time (s)')
show()

#loglog(sizes,times)
#title('SNES ex5')
#xlabel('Problem Size $N$')
#ylabel('Time (s)')
#show()
