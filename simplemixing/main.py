#! /usr/bin/env python
import os

sizes_simplemixing = []
sizes_newtonls = []
times_simplemixing = []
times_newtonls = []
for k in range(5): 
	Nx	=10 *2**k
	modname	='perf%d'%k
	options	=['-da_grid_x',str(Nx),'-da_grid_y',str(Nx),'-log_view',':%s.py:ascii_info_detail' %modname,'-simple_mixing']
	os.system('./bin/ex5 '+' '.join(options))
	perfmod	=__import__(modname)
	sizes_simplemixing.append(Nx ** 2)
	times_simplemixing.append(perfmod.Stages['Main Stage']['SNESSolve'][0]['time'])
	modname	='perf_new%d'%k
	options	=['-da_grid_x',str(Nx),'-da_grid_y',str(Nx),'-log_view',':%s.py:ascii_info_detail' %modname,'-snes_type','newtonls','-snes_rtol','1e-8']
	os.system('./bin/ex5 '+' '.join(options))
	perfmod	=__import__(modname)
	sizes_newtonls.append(Nx ** 2)
	times_newtonls.append(perfmod.Stages['Main Stage']['KSPSolve'][0]['time'])
	
	#print zip(sizes,times)
	#print zip(':%s.py:ascii_info_detail' %modname)

from pylab import legend, plot, loglog, show,title,xlabel,ylabel
p1, =plot(sizes_simplemixing,times_simplemixing)
p2, =plot(sizes_newtonls,times_newtonls)
legend([p1,p2],["Simplemixing","Newtonls"])
title('Linear solver time')
xlabel('Problem Size $N$')
ylabel('Time (s)')
show()

p1, =loglog(sizes_simplemixing,times_simplemixing)
p2, =loglog(sizes_newtonls,times_newtonls)
legend([p1,p2],["Simplemixing","Newtonls"])
title('Linear solver time (loglog)')
xlabel('Problem Size $N$')
ylabel('Time (s)')
show()

#loglog(sizes,times)
#title('SNES ex5')
#xlabel('Problem Size $N$')
#ylabel('Time (s)')
#show()
